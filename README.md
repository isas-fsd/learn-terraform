# Terra from EPFL to SwitchEngines

Le but de ce dépôt est de tester [Terraform] sur [SwitchEngines].

> [Terraform] est à l'infrastructure ce qu'[Ansible] est au déploiement.

En d'autres termes, l'idempotence de l'infrastructure peut être atteinte avec
[Terraform]. Les commandes `terraform plan`, `terraform apply` et 
`terraform destroy` permettent respectivement de :
  * `terraform plan`  
    Équivalent du `--check` ou `--dry-run` ; dit ce qui est prévu, par exemple :  
    `Plan: 1 to add, 0 to change, 0 to destroy.` ou encore 
    `Terraform has compared your real infrastructure against your configuration 
     and found no differences, so no changes are needed.`.
  * `terraform apply` et `terraform destroy` sont "self-explanatory".


## QuickStart

  1. [install-terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli#install-terraform)
  1. [clouds.yaml](https://engines.switch.ch/horizon/project/api_access/)  
     ![](cloudyaml.jpg)  
     and save it in your working directory
  1. create the configuration file `main.tf`  
    a. the `terraform` should have the `required_providers` `openstack`  
    b. a `provider` section should configure the provider (i.e. reference the `clouds.yaml` file)  
    c. use resource blocks to define components of your infrastructure.
  1. run `terraform init` (will install the required providers)
  1. run `terraform plan`
  1. run `terraform apply`


## Remarques

  * [Terraform] a un provider pour [Kubernetes] : https://registry.terraform.io/providers/hashicorp/kubernetes/2.13.1
  * [GitLab] a des intégrations de [Kubernetes] et [Terraform] → "GitLab has deep integrations with Terraform to run Infrastructure as Code pipelines and support various processes." : https://gitlab.com/help/user/infrastructure/index


## Liens

  * [SwitchEngines] API Access : https://engines.switch.ch/horizon/project/api_access/
  * [Terraform] [OpenStack] provider : 
    * registry : https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest
    * documentation : https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs
    * code : https://github.com/terraform-provider-openstack/terraform-provider-openstack
  * Composant Terraform `openstack_compute_instance_v2` : https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/compute_instance_v2
  * How to Configure Terraform to Automate OpenStack’s Resources : https://openmetal.io/docs/edu/openstack/how-to-configure-terraform-to-automate-openstacks-resources/


<!-- REFERENCES -->
[Terraform]: https://www.terraform.io
[SwitchEngines]: https://engines.switch.ch
[OpenStack]: https://www.openstack.org
[Ansible]: https://www.ansible.com
[Kubernetes]: https://kubernetes.io
[GitLab]: https://gitlab.com
