# Define required providers
terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.35.0"
    }
  }
}

# Configure the OpenStack Provider
provider "openstack" {
  cloud = "engines"
}

resource "openstack_compute_instance_v2" "my_instance" {
  name      = "my_test_instance"
  region    = "LS"
  image_id  = "4124a238-cd53-4d01-af1a-48610d12195a"
  flavor_id = "f7dfa123-bf67-4965-8db1-9231701c0c1a"
  key_pair  = "nbo"
  # 
  # network {
  #   uuid = "00000000-0000-0000-0000-000000000000"
  #   name = "public"
  # }
  # 
  # network {
  #   uuid = "11111111-1111-1111-1111-111111111111"
  #   name = "private"
  # }
}
